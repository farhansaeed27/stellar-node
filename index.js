const StellarSdk = require("stellar-sdk");
const express = require("express");
const app = express();

const mongoose = require("mongoose");
const logger = require("morgan");

const { errors } = require("celebrate");
const bodyParser = require("body-parser");
const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");

const server = new StellarSdk.Server("https://horizon-testnet.stellar.org");

global.server = server;

mongoose.connect(
  "mongodb://farhan:farhan12@ds117858.mlab.com:17858/stellardb",
  { useUnifiedTopology: true, useNewUrlParser: true }
);

app.use(logger("dev"));
app.use(bodyParser.json());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use("/", indexRouter);
app.use("/users", usersRouter);

app.use(errors());

app.listen(3030, () => {
  console.log("Localhost:3030");
});
