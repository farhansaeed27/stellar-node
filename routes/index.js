var express = require("express");
var router = express.Router();
const paginTokenSchema = require("../models/pagingToken");
const adminWalletSchema = require("../models/admin_wallet");
const transactionSchema = require("../models/transactions");
const userSchema = require("../models/user");
const walletSchema = require("../models/wallet");
const deposit = require("../util/withdraw");
/* GET home page. */
const StellarSdk = require("stellar-sdk");
const fetch = require("node-fetch");

router.get("/", function(req, res, next) {
  res.render("index", { title: "Express" });
});

router.get("/createaccount", async (req, res) => {
  try {
    const pair = StellarSdk.Keypair.random();
    const response = await fetch(
      `https://friendbot.stellar.org?addr=${encodeURIComponent(
        pair.publicKey()
      )}`
    );
    const responseJSON = await response.json();
    console.log("SUCCESS! You have a new account :)\n", responseJSON);
    res.json({ pubk: pair.publicKey(), secret: pair.secret() });
  } catch (e) {
    console.error("ERROR!", e);
  }
});

router.get("/getaccount", async (req, res) => {
  let account = await server.loadAccount(
    "GDI73VPE5TE5476YNLEXVRRCB2F25DUJ2FCTXWZMR6RLRDSOPHYQHMZC"
  );
  console.log(account);
  res.send("asdasd");
});
// GDI73VPE5TE5476YNLEXVRRCB2F25DUJ2FCTXWZMR6RLRDSOPHYQHMZC
router.get("/latestpayments", async (req, res) => {
  let adminWallet = await adminWalletSchema.findOne({});
  if (!adminWallet) res.json({ error: { message: "Wallet Not Found" } });
  let { pubk } = adminWallet;
  let payments = server.payments().forAccount(pubk);
  var lastToken = await loadLastPagingToken();
  if (lastToken && lastToken.token) {
    payments.cursor(lastToken.token);
  }
  payments.stream({
    onmessage: async function(payment) {
      // Record the paging token so we can start from here next time.
      await savePagingToken(lastToken._id, payment.paging_token);

      if (payment.to !== pubk) {
        return;
      }
      let hash = payment.transaction_hash;
      // const { to, value } = await transactionSchema.findOne({ tx: hash });
      // await deposit(to, value)
      //   .then(data => {
      //     console.log(data);
      //     console.log("Successfully Receiverd");
      //   })
      //   .catch(err => {
      //     console.log("Error On Receiving");
      //   });
      var asset;
      if (payment.asset_type === "native") {
        asset = "lumens";
      } else {
        asset = payment.asset_code + ":" + payment.asset_issuer;
      }
      let transaction = await payment.transaction();
      let user = await userSchema.findOne({ memo: transaction.memo });
      if (!user) {
        console.log("Invalid Memo");
        return;
      } else {
        let amount = Number(payment.amount);
        let userWallet = await walletSchema.findOne({
          userId: user._id
        });
        console.log(userWallet)
        userWallet.balance += amount;
        await userWallet.save();
      }
    },

    onerror: function(error) {
      console.error("Error in payment stream");
    }
  });
  async function savePagingToken(id, token) {
    await paginTokenSchema.findOneAndUpdate({ _id: id }, { token });
  }

  async function loadLastPagingToken() {
    let token = await paginTokenSchema
      .findOne({})
      .lean()
      .exec();
    console.log(token);
    return token;
  }
});

module.exports = router;
