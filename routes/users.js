var express = require("express");
var router = express.Router();
const userSchema = require("../models/user");
const adminSchema = require("../models/admin");
const walletSchema = require("../models/wallet");
const adminWalletSchema = require("../models/admin_wallet");
const { validateSignup, validateLogin } = require("../validation");
const StellarSdk = require("stellar-sdk");
const deposit = require("../util/deposit");
const withdraw = require("../util/withdraw");
const createAccount = require("../util/createAccount");

router.get("/", function(req, res, next) {
  res.send("respond with a resource");
});
router.post("/login", validateLogin(), async (req, res) => {
  let { email, password } = req.body;
  userSchema
    .findByCredentials(email, password)
    .then(user => {
      return user.generateAuthToken().then(token => {
        res.header("x-auth", token).json({ token });
      });
    })
    .catch(e => {
      console.log(e);
      res.status(400).send({ error: { message: e } });
    });
});

router.post("/deposit", async (req, res) => {
  let { sender, receiver, value } = req.body;
  deposit(sender, receiver, value)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      console.log(err);
    });
});
router.post("/withdraw", async (req, res) => {
  let { sender, receiver, value, memo } = req.body;
  withdraw(sender, receiver, value, memo)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      console.log(err);
    });
});
router.post("/signup", validateSignup(), (req, res) => {
  let { email, password } = req.body;
  let newUser = new userSchema({ email, password });
  newUser.save().then(async user => {
    const pair = StellarSdk.Keypair.random();
    let log = await createAccount(pair.publicKey(), 100).catch(err => {
      console.log(err);
    });
    let newWallet = new walletSchema({
      userId: user._id,
      pubk: pair.publicKey(),
      secret: pair.secret()
    });
    newWallet.save().then(wallet => {
      user.generateAuthToken().then(token => {
        res.status(200).json({ token });
      });
    });
  });
});

router.post("/signupadmin", validateSignup(), (req, res) => {
  let { email, password } = req.body;
  let newAdmin = new adminSchema({ email, password });
  newAdmin.save().then(async user => {
    let account = await newAccount();
    let newAdminWallet = new adminWalletSchema({
      userId: user._id,
      ...account
    });
    newAdminWallet.save().then(wallet => {
      user.generateAuthToken().then(token => {
        res.status(200).json({ token });
      });
    });
  });
});

module.exports = router;
