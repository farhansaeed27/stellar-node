const userSchema = require("../models/user");
const adminSchema = require("../models/admin");
const walletSchema = require("../models/wallet");
const adminWalletSchema = require("../models/admin_wallet");
const StellarSdk = require("stellar-sdk");
const transactionSchema = require("../models/transactions");

function withdraw(sender, receiver, value) {
  return new Promise(async (res, rej) => {
    value = String(value);
    let adminWallet = await adminWalletSchema.findOne({});
    // Math.floor(Math.random() * 1000000000)
    // let memo = Math.floor(Math.random() * 1000000000).toString();
    let user = await userSchema.findOne({ _id: receiver });
    let fromWallet = await walletSchema.findOne({ userId: sender });
    let fromacc = await StellarSdk.Keypair.fromSecret(fromWallet.secret);
    if (fromWallet.balance < Number(value)) {
      return res.json({ error: { message: "Insufficiant Balance" } });
    }
    let transaction;
    server
      .loadAccount(adminWallet.pubk)
      .catch(StellarSdk.NotFoundError, err => {
        return rej("Account Not Found");
      })
      .then(() => {
        return server.loadAccount(fromacc.publicKey());
      })
      .then(sourceAccount => {
        transaction = new StellarSdk.TransactionBuilder(sourceAccount, {
          fee: StellarSdk.BASE_FEE,
          networkPassphrase: StellarSdk.Networks.TESTNET
        })
          .addOperation(
            StellarSdk.Operation.payment({
              destination: adminWallet.pubk,
              asset: StellarSdk.Asset.native(),
              amount: value
            })
          )
          .addMemo(StellarSdk.Memo.text(user.memo))
          .setTimeout(180)
          .build();

        transaction.sign(fromacc);
        return server.submitTransaction(transaction);
      })
      .then(result => {
        transactionSchema
          .create({ tx: result.hash, from: sender, to: receiver, value })
          .then(newtx => {
            fromWallet.balance -= Number(value);
            fromWallet.save().then(userresp => {
              adminWallet.balance += Number(value);
              adminWallet.save().then(adminresp => {
                res(result);
              });
            });
          });
      });
  });
}
module.exports = withdraw;
