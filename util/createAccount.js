const StellarSdk = require("stellar-sdk");
const adminWalletSchema = require("../models/admin_wallet");


function createAccount(destination, initial = 1) {
  return new Promise(async (res, rej) => {
    let adminWallet = await adminWalletSchema.findOne({});
    let fromacc = await StellarSdk.Keypair.fromSecret(adminWallet.secret);
    if (adminWallet.balance < initial) {
      return res.json({ error: { message: "Insufficiant Balance" } });
    }

    let transaction;

    server
      .loadAccount(fromacc.publicKey())
      .then(sourceAccount => {
        transaction = new StellarSdk.TransactionBuilder(sourceAccount, {
          fee: StellarSdk.BASE_FEE,
          networkPassphrase: StellarSdk.Networks.TESTNET
        })
          .addOperation(
            StellarSdk.Operation.createAccount({
              destination: destination,
              startingBalance: String(initial)
            })
          )
          .addMemo(StellarSdk.Memo.text("Account Creation"))
          .setTimeout(180)
          .build();

        transaction.sign(fromacc);
        return server.submitTransaction(transaction);
      })
      .then(result => {
        res(result);
      });
  });
}

module.exports = createAccount;
