const userSchema = require("../models/user");
const adminSchema = require("../models/admin");
const walletSchema = require("../models/wallet");
const adminWalletSchema = require("../models/admin_wallet");
const StellarSdk = require("stellar-sdk");
const transactionSchema = require("../models/transactions");

function withdraw(sender, receiver, value, memo = "") {
  return new Promise(async (res, rej) => {
    value = String(value);
    let adminWallet = await adminWalletSchema.findOne({});
    if (!adminWallet) {
      return res({ error: { message: "Admin Wallet Not Found" } });
    }
    let toWallet = await walletSchema.findOne({ pubk: receiver });
    let senderWallet = await walletSchema.findOne({ userId: sender });
    if (!senderWallet) {
      return res({ error: { message: "Sender Wallet Not Found" } });
    }
    let fromacc = await StellarSdk.Keypair.fromSecret(adminWallet.secret);
    if (receiver == adminWallet.pubk) {
      return res({ error: { message: "Can't withdraw to Admin" } });
    }
    if (senderWallet.balance < Number(value)) {
      return res({
        error: { message: "Insufficiant Balance in Sender Balance" }
      });
    }
    if (adminWallet.balance < Number(value)) {
      return res.json({ error: { message: "Insufficiant Balance" } });
    }
    let transaction;
    server
      .loadAccount(receiver)
      .catch(StellarSdk.NotFoundError, err => {
        return rej("Account Not Found");
      })
      .then(() => {
        return server.loadAccount(fromacc.publicKey());
      })
      .then(sourceAccount => {
        transaction = new StellarSdk.TransactionBuilder(sourceAccount, {
          fee: StellarSdk.BASE_FEE,
          networkPassphrase: StellarSdk.Networks.TESTNET
        })
          .addOperation(
            StellarSdk.Operation.payment({
              destination: receiver,
              asset: StellarSdk.Asset.native(),
              amount: value
            })
          )
          .addMemo(StellarSdk.Memo.text(memo))
          .setTimeout(180)
          .build();

        transaction.sign(fromacc);
        return server.submitTransaction(transaction);
      })
      .then(result => {
        if (toWallet) {
          toWallet.balance += Number(value);
          toWallet.save().then(userresp => {
            senderWallet.balance -= value;
            senderWallet.save().then(() => {
              // adminWallet.balance -= value;
              // adminWallet.save().then(adminresp => {
              //   res(result);
              // });
            });
          });
        } else {
          senderWallet.balance -= value;
          senderWallet.save().then(() => {
            adminWallet.balance -= value;
            adminWallet.save().then(adminresp => {
              res(result);
            });
          });
        }
      });
  });
}

module.exports = withdraw;
