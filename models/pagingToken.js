const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const PagingToken = new Schema({
  token: { type: String, required: true, default: "0" }
});

module.exports = mongoose.model("pagingtoken", PagingToken, "pagingtoken");
