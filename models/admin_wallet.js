const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const adminWalletSchema = new Schema({
  userId: { type: Schema.Types.ObjectId, ref: "admin" },
  pubk: { type: String, required: true },
  secret: { type: String, required: true },
  balance: { type: Number, default: 10000 },
  coin: { type: String, default: "XLM" }
});
module.exports = mongoose.model("adminWallet", adminWalletSchema);
