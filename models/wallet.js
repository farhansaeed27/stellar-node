const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const walletSchema = new Schema({
  userId: { type: Schema.Types.ObjectId, ref: "users" },
  pubk: { type: String, required: true },
  secret: { type: String, required: true },
  balance: { type: Number, default: 100 },
  coin: { type: String, default: "XLM" }
});
module.exports = mongoose.model("wallets", walletSchema);
