const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const transactionSchema = new Schema({
  tx: { type: String, required: true },
  to: { type: Schema.Types.ObjectId, required: true },
  from: { type: Schema.Types.ObjectId, required: true },
  value: { type: Number, required: true }
});

module.exports = mongoose.model("transactions", transactionSchema);
